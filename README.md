## GKE Cluster 생성 (defaultpool : 3, 확장노드:5)

## 실습 환경 확인
 - k8s 클러스터
```bash
# 1. 1.15.12-gke.2(default,6대)로 구성

# kubectl 확인
$ kubectl get nodes

# helm Install
$ wget https://get.helm.sh/helm-v3.3.0-linux-amd64.tar.gz
$ tar xvzf ./helm-v3.3.0-linux-amd64.tar.gz
$ cd linux-amd64
$ sudo cp ./helm /usr/local/bin/
$ helm version

```

## 실습 환경 설정
```bash
mkdir sample
cd sample
wget [다운로드경로]
unzip *
```


## MariaDB 설치
 - [참조] 일반적인 Helm 설치 방법
```bash
# helmChart 검색: https://hub.helm.sh/

# Helm Repository 확인
$ helm repo list

# Helm Repository 추가
$ helm repo add bitnami https://charts.bitnami.com/bitnami

# HelmChart 검색
$ helm search repo mariadb

# HelmChart Pull
$ helm pull bitnami/mariadb

# 압축 해제 후 values.yaml 수정
$ tar xvzf ./mariadb-7.7.2.tgz

# HelmChart Install/UnInstall
$ helm install mariadb ./mariadb
$ helm uninstall mariadb
```

 - MariaDB Install
```bash
$ helm install mariadb ./mariadb -f ./mariadb/values.yaml
$ kubectl get all
$ helm ls
$ kubectl logs -f mariadb-0
```

---

## Jenkins 설치
```bash
# [참조]
# helmChart 검색: https://hub.helm.sh/
# helm repo add stable https://kubernetes-charts.storage.googleapis.com
# helm search repo jenkins
# helm pull stable/jenkins

$ kubectl create namespace jenkins
$ helm install jenkins ./jenkins -f ./jenkins/values.yaml -n jenkins
$ helm ls -n jenkins
$ kubectl get all -n jenkins

# Jenkins 접속(admin/admin)
```

## Jenkins 설정
- Git 설정
```bash
# https://github.com/ws0110/petclinic
Jenkins 관리 > Manage Credentials > (global)의 'add credentials'
Username: ws010110@gmail.com
Password: 0825test!
ID: git-cred
```

- DockerHub 설정
```bash
Jenkins 관리 > Manage Credentials > (global)의 'add credentials'
Username: 'DockerHub 계정'(jangwisu)
Password: 'DockerHub 비번'
ID: dockerhub-cred
```


- ClusterRoleBinding 정의
```bash
# "default"계정 권한 설정
#  - jenkins에서 helm 실행 시 기본적으로 해당 네임스페이스('jenkins')의 "default" 계정(service account)을 통해 배포
#    → default 계정에게 리소스를 배포/조회 등 할 수 있는 권한 부여

$ kubectl apply -f jenkins_clusterRoleBinding.yaml
$ kubectl get clusterRoleBinding | grep cluster-admin-jenkins-helm
```

- jenkinsfile
```Groovy
podTemplate(
  label: 'petclinic', 
	containers: [
		containerTemplate(name: "docker", image: "docker:stable", ttyEnabled: true, command: "cat"),
		containerTemplate(name: "helm", image: "dtzar/helm-kubectl", ttyEnabled: true, command: "cat")
	],
	volumes: [
		hostPathVolume(hostPath: "/var/run/docker.sock", mountPath: "/var/run/docker.sock")
	]
    
) {
    node('petclinic') {
        def moduleName = "app"
        def gitUrl = "https://github.com/ws0110/petclinic"
        def imgRepoHost = "https://registry.hub.docker.com"
        def imgRepo = "jangwisu/app"  // ## DockerHub 계정확인 ##
        def deployNamespace = "default"
        def helmReleaseName = "app"
        def imageTag
        
        try {
            stage('Git Check Out'){
              git url: "${gitUrl}", 
                    branch: 'master', 
                    credentialsId: 'git-cred', 
                    changelog: false, 
                    poll: false
                gitCommit = sh(returnStdout: true, script: 'git rev-parse --short HEAD').trim()
                imageTag = "${BUILD_NUMBER}-${gitCommit}"
                echo "imageTag = ${imageTag}"
            }
            stage('MVN Build - web'){
		    		   sh "./mvnw package"  
            }
            stage("Image Build") {
							container("docker") {
								sh "docker build -t ${imgRepo}:${imageTag} ."
							}
						}
            stage('Push Image'){ 
                container('docker') {
                    docker.withRegistry("${imgRepoHost}", 'dockerhub-cred') {
                        docker.image("${imgRepo}:${imageTag}").push()
                    }
                }
            }
            stage('Helm Deploy'){ 
                container('helm') {
	                overrides = "--set image.java.tag=${imageTag} --set image.java.repository=${imgRepo}"
                    sh "helm upgrade --install ${helmReleaseName} ./helmchart -f ./helmchart/values.yaml ${overrides} --namespace=${deployNamespace}"
                }
            }
            echo "result: ${currentBuild.currentResult}"
        } catch(e) {
            currentBuild.result = "FAILED"
            echo "result: ${currentBuild.currentResult}"
            // println e
            throw e
        }
    }
}
```
- 빌드 실행
```bash
# Jenkins 빌드 실행

# 배포 pod 생성 확인
$ kubectl get pod -n jenkins

# jenkinsfile 확인 및 빌드 console 확인

# helm App 배포 확인
$ helm ls
$ kubectl get all 

# app 접속
```

---

## 모니터링(Prometheus/Grafana)
```bash
$ kubectl create namespace monitoring

$ helm install prometheus --namespace monitoring ./prometheus-operator
$ kubectl get all -n monitoring

# promethues 접속
# grafana 접속
 - 기본 대시보드 확인

# 참조: https://grafana.com/grafana/dashboards
# Mariadb ServiceMonitor Enable
$ helm upgrade mariadb ./mariadb --set metrics.enabled=true
$ kubectl get servicemonitor

# operator패턴
```

---

## 로깅(EFK)
- Elasticsearch/Kibana Install
```bash
$ kubectl create namespace logging

$ helm install elastic --namespace logging ./elasticsearch
$ helm ls -n logging
$ kubectl get all -n logging
```

- Fluent-bit Install
```bash
$ helm install fluent-bit --namespace logging ./fluent-bit
$ helm ls -n logging
$ kubectl get all -n logging

# kibana 확인
 - Stack Management > Index Patterns > 'Create index pattern' 
   → 'kubernetes_cluster-*' Index 
   → 'Next step' 버튼 클릭 
   → @timestamp 선택
 - Search: kubernetes.labels.app:petclinic
```


---   

## URL
## https://cloud.google.com
## https://bitbucket.org/ws0110/k8s-tool
## https://hub.docker.com/
